<?php

/**
 * @file
 * Imports the CSV file, Exports to CSV or deletes the
 * taxonomy vocabulary terms chosen.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\taxonomy\Entity\Term;

/**
 * Returns the taxonomy_csv value from the form.
 */
function _import_terms_form(array &$form, FormStateInterface $form_state, $vocabulary) {
  $fid = $form_state->getValue('taxonomy_csv');
  $delimiter = $form_state->getValue('delimiter');
  _import_terms($fid, $vocabulary, $delimiter);
}

/**
 * Imports Taxonomy Terms.
 */
function _import_terms($fid, $vocabulary, $delimiter) {
  if (PHP_SAPI == 'cli') {
    $fileurl = $fid;
    $filename = basename($fileurl);
  }
  else {
    $file = File::load($fid[0]);
    $filename = $file->getFilename();
    $fileuri = $file->getFileUri();
    $fileurl = file_create_url($fileuri);
  }

  if (PHP_SAPI == 'cli') {
    if (drush_confirm('Are you sure you want to import the terms into the selected vocabulary?')) {
      drupal_set_message(t("
        ---------------------------------------------------------------------------------------
          Importing: @filename into @vocabulary",
        [
          '@filename' => $filename,
          '@vocabulary' => $vocabulary,
        ]));
    }
    else {
      drush_user_abort();
      return;
    }
  }
  else {
    drupal_set_message('Importing: ' . $filename . ' into ' . $vocabulary);
  }
  $terms_created = 0;
  $term_exists = 0;

  $handle = fopen($fileurl, "r");
  $headers = fgetcsv($handle);

  while (($line = fgetcsv($handle)) !== FALSE) {
    foreach ($line as $i => $field) {
      $record[$headers[$i]] = $field;
    }
    $path = explode($delimiter, $record['name']);

    $level = 0;

    foreach ($path as $name) {
      // Get parents from our term we will search for.
      $terms_parents = $path;
      $position = array_search($name, $terms_parents);
      if ($position !== FALSE) {
        array_splice($terms_parents, ($position + 1));
      }
      // Check if the term exists in the vocabulary.
      $term_check = taxonomy_term_load_multiple_by_name($name, $vocabulary);
      // Checking to see if the parents match.
      if (count($term_check) >= 1) {
        $parent_check_array = [];
        $parent_match = TRUE;
        // Check all terms whose name matches our current term.
        foreach ($term_check as $parent_term) {
          // Get the terms id to check.
          $key = $parent_term->id();
          // Load the terms data from it's id.
          $parent_storage = \Drupal::entityTypeManager()
            ->getStorage('taxonomy_term')
            ->loadAllParents($key);
          // Iterate through the term data.
          foreach ($parent_storage as $parent) {
            array_push($parent_check_array, $parent->getName());
          }
          $parent_check_array = array_reverse($parent_check_array);
          // Check if parents of term match parents of identical term found.
          if ($parent_check_array == $terms_parents) {
            // The parents matched so no need to create a term.
            $parent_match = TRUE;
          }
          else {
            // The parents did not match so we need to create a term.
            $parent_match = FALSE;
          }
        }
      }
      else {
        $parent_match = FALSE;
      }
      // If the term already exists and the parents match.
      if ($parent_match == TRUE) {
        $term_exists++;
      }
      else {
        if ($name == $path[0]) {
          // Create the parent term.
          $term = Term::create([
            'name' => $name,
            'vid' => $vocabulary,
          ]);
          $term->save();
        }
        else {
          $previous_level = $level - 1;
          // Get the tid of the parent term.
          $parent_term = \Drupal::entityTypeManager()
            ->getStorage('taxonomy_term')
            ->loadByProperties([
              'vid' => $vocabulary,
              'name' => $path[$previous_level],
            ]);
          $term = reset($parent_term);
          $term_id = $term->id();
          // Create the term with it's parent.
          $term = Term::create([
            'name' => $name,
            'vid' => $vocabulary,
            'parent' => $term_id,
          ]);
          $term->save();
          $parent_term = NULL;

        }
        $terms_created++;
      }
      $level++;
    }

  }

  if ($terms_created > 0) {
    if (PHP_SAPI == 'cli') {
      drupal_set_message(t("
          File completed. @terms_created terms created.
        ---------------------------------------------------------------------------------------",
        [
          '@terms_created' => $terms_created,
        ]));
    }
    else {
      drupal_set_message('File completed. ' . $terms_created . ' terms created.');
    }
  }
  else {
    if (PHP_SAPI == 'cli') {
      drupal_set_message(t("
          File completed. No terms were created.
        ---------------------------------------------------------------------------------------"
      ));
    }
    else {
      drupal_set_message('File completed. No terms were created.');
    }
  }

  if ($term_exists > 0) {
    if (PHP_SAPI == 'cli') {
      drupal_set_message(t("
          There were @term_exists terms in the csv that were already in this vocabulary and so were not imported.
        ---------------------------------------------------------------------------------------",
        [
          '@term_exists' => $term_exists,
        ]));
    }
    else {
      drupal_set_message('There were ' . $term_exists . ' terms in the csv that were already in this vocabulary and so were not imported.');
    }
  }
}

/**
 * Deletes Taxonomy Terms.
 */
function _delete_terms($vocabulary) {

  if (PHP_SAPI == 'cli') {
    if (drush_confirm('Are you sure you want to delete the terms from the selected vocabulary?')) {

    }
    else {
      drush_user_abort();
      return;
    }
  }


  $tids = \Drupal::entityQuery('taxonomy_term')
    ->condition('vid', $vocabulary)
    ->execute();

  $controller = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
  $entities = $controller->loadMultiple($tids);
  $controller->delete($entities);
  if (PHP_SAPI == 'cli') {
    drupal_set_message(t("
        ---------------------------------------------------------------------------------------
          All terms in the @vocabulary vocabulary have been deleted.
        ---------------------------------------------------------------------------------------",
      [
        '@vocabulary' => $vocabulary,
      ]));
  }
  else {
    drupal_set_message('All terms in the ' . $vocabulary . ' vocabulary have been deleted.');
  }
}

/**
 * Exports Taxonomy Terms to a csv.
 */
function _export_terms($vocabulary, $delimiter) {

  if (PHP_SAPI == 'cli') {
    if (drush_confirm('Are you sure you want to export the terms from the selected vocabulary?')) {

    }
    else {
      drush_user_abort();
      return;
    }
  }

  $directory = "public://export/";
  $filename = "taxonomy_output";
  $_filename_key = '';
  $date = date('y-m-d_h_i');

  // Pulling in all the terms from the vocabulary.
  $terms = \Drupal::service('entity_type.manager')
    ->getStorage("taxonomy_term")
    ->loadTree($vocabulary, $parent = 0, $max_depth = NULL, $load_entities = FALSE);

  $tree = [];

  // Attempting to get multidimensional array of terms.
  foreach ($terms as $tree_object) {
    _build_tree($tree, $tree_object, $vocabulary);
  }

  // Create the file.
  if ($_filename_key != '') {
    $date = $_filename_key . "_" . $date;
  }
  $outfilename = $directory . $filename . "_" . $vocabulary . "_" . $date . ".csv";
  $printname = $filename . "_" . $vocabulary . "_" . $date . ".csv";
  $f = fopen($outfilename, "w");

  // Write header.
  fwrite($f, "name\n");

  $firstterm = 0;
  $lastterm = 0;

  // Output terms to csv.
  foreach ($tree as $term) {
    $tid = $term->tid;
    $singleterm = Term::load($tid);
    $name = $singleterm->getName();
    if ($term->depth > 0) {
      if (($term->depth !== $lastterm) && ($term->depth > $lastterm)) {

        // If the terms has a unique depth we'll output it inline with parent.
        fwrite($f, $delimiter . $name);
      }
      else {

        // If the terms depth matches the previous term, we need a new line and
        // to rebuild the lineage.
        $parent = \Drupal::entityTypeManager()
          ->getStorage("taxonomy_term")
          ->loadParents($tid);
        $parentname = $parent[key($parent)]->getName();
        $parenttid = $parent[key($parent)]->id();
        $parentpath = $parentname . $delimiter;

        // Get deeper levels if available.
        while ($parenttid) {
          $parent = \Drupal::entityTypeManager()
            ->getStorage("taxonomy_term")
            ->loadParents($parenttid);
          if ($parent) {
            $parentname = $parent[key($parent)]->getName();
            $parenttid = $parent[key($parent)]->id();
            $parentpath = $parentname . $delimiter . $parentpath;
          }
          else {
            $parenttid = NULL;
          }
        }
        fwrite($f, "\n" . $parentpath . $name);
      }
      $lastterm = $term->depth;
    }
    elseif ($term->depth == 0 && $firstterm == 0) {
      fwrite($f, $name);
      $firstterm++;
    }
    else {
      fwrite($f, "\n" . $name);
    }
  }

  // Close the csv file.
  fclose($f);

  // Check if file exists and if does output a message with download link.
  if (!file_exists($outfilename)) {
    drupal_set_message('Error writing to file ' . $outfilename);
  }
  else {
    $fileurl = file_create_url($outfilename);
    if (PHP_SAPI == 'cli') {
      drupal_set_message(t("
        ---------------------------------------------------------------------------------------
          Terms have been exported. File created with name: <a href = \"@url\">@link.</a>
        ---------------------------------------------------------------------------------------",
      [
        '@url' => $fileurl,
        '@link' => $printname,
      ]));
    }
    else {
      drupal_set_message(t('Terms have been exported. File created with name: <a href = "@url">@link.</a>', [
        '@url' => $fileurl,
        '@link' => $printname,
      ]));
    }

  }
}

/**
 * Populates a tree array given a taxonomy term tree object.
 */
function _build_tree(&$tree, $object, $vocabulary) {

  $tree[$object->tid] = $object;
  $tree[$object->tid]->children = [];
  $object_children = &$tree[$object->tid]->children;

  $children = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren($object->tid);
  if (!$children) {
    return;
  }

  $child_tree_objects = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vocabulary, $object->tid);

  foreach ($children as $child) {
    foreach ($child_tree_objects as $child_tree_object) {
      if ($child_tree_object->tid == $child->id()) {
        _build_tree($object_children, $child_tree_object, $vocabulary);
      }
    }
  }
}
