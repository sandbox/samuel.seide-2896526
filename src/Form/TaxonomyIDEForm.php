<?php

namespace Drupal\taxonomy_i_d_e\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\file\Entity\File;

/**
 * Taxonomy IDE Config form.
 */
class TaxonomyIDEForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'TaxonomyIDEForm';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get a list of all the taxonomy vocabularies for use in the drop down.
    $query = \Drupal::entityQuery('taxonomy_vocabulary');
    $all_ids = $query->execute();
    $vocs = [$this->t('--- SELECT ---')];
    foreach (Vocabulary::loadMultiple($all_ids) as $vocabulary) {
      $vocs[$vocabulary->id()] = $this->t('%label', ['%label' => $vocabulary->label()]);
    }

    $form['taxonomy_select'] = [
      '#type' => 'select',
      '#title' => $this->t('Taxonomy Vocabulary Target'),
      '#description' => $this->t('This is the taxonomy that you want to do an action on.'),
      '#options' => $vocs,
    ];

    $form['action_item'] = [
      '#type' => 'select',
      '#title' => $this->t('Action to take'),
      '#options' => [
        'select' => $this->t('--- SELECT ---'),
        'add' => $this->t('Import Taxonomy Vocabulary Nodes'),
        'delete' => $this->t('Delete Taxonomy Vocabulary Nodes'),
        'export' => $this->t('Export Taxonomy Vocabulary to CSV'),
      ],
      '#states' => [
        'invisible' => [
          ':input[name="taxonomy_select"]' => ['value' => 0],
        ],
      ],
    ];

    $form['delimiter'] = [
      '#type' => 'select',
      '#title' => $this->t('CSV Delimiter'),
      '#description' => $this->t('Choose a delimiter used on each line for parent child relationships'),
      '#options' => [
        ' ---> ' => $this->t(' ---> '),
      ],
      '#states' => [
        'visible' => [
          ':input[name="action_item"]' => [
            ['value' => 'add'],
            ['value' => 'export']
          ],
        ],
      ],
    ];

    $form['taxonomy_csv'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Taxonomy Terms CSV'),
      '#description' => $this->t('upload a csv file of terms showing relationship by using delimiter'),
      '#upload_location' => 'public://import/',
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
      '#states' => [
        'visible' => [
          ':input[name="action_item"]' => ['value' => 'add'],
        ],
      ],
    ];

    $form['actions']['#type'] = 'actions';

    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
      '#button_type' => 'primary',
      '#attributes' => array('onclick' => 'if(!confirm("This will delete the terms from the selected taxonomy. Continue?")){return false;}'),
      '#states' => [
        'visible' => [
          ':input[name="action_item"]' => [
            ['value' => 'delete']
          ],
        ],
      ],
    ];

    $form['actions']['import'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#button_type' => 'primary',
      '#attributes' => array('onclick' => 'if(!confirm("This will import the terms to the selected taxonomy. Continue?")){return false;}'),
      '#states' => [
        'visible' => [
          ':input[name="action_item"]' => [
            ['value' => 'add']
          ],
        ],
      ],
    ];

    $form['actions']['export'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export'),
      '#button_type' => 'primary',
      '#attributes' => array('onclick' => 'if(!confirm("This will export the terms from the selected taxonomy to a csv. Continue?")){return false;}'),
      '#states' => [
        'visible' => [
          ':input[name="action_item"]' => [
            ['value' => 'export']
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $vocabulary = $form_state->getValue('taxonomy_select');
    $action = $form_state->getValue('action_item');

    if (!$vocabulary) {
      $form_state->setErrorByName('taxonomy_select', $this->t("You must choose a value for this field other than 'select'"));
    }

    switch ($action) {
      case "add":
        // Check file for proper formatting.
        // Set variables for the filename and file uri coming from the form.
        $fid = $form_state->getValue('taxonomy_csv');
        $file = File::load($fid[0]);
        $filename = $file->getFilename();
        $fileuri = $file->getFileUri();
        $fileurl = file_create_url($fileuri);

        // Open the file.
        if ($handle = fopen($fileurl, 'r')) {

          if ($line = fgetcsv($handle, 4096)) {

            // Validate the headers.
            if ($line[0] !== 'name') {
              $form_state->setError($form, $this->t("@filename must have a column header value of 'name'.", ['@filename' => $filename]));
            }

            fclose($handle);
          }
          else {
            $form_state->setError($form, $this->t("@filename is not a properly formatted csv file.", ['@filename' => $filename]));
          }
        }
        break;

      case "select":
        $form_state->setErrorByName('action_item', $this->t("You must choose a value for this field other than 'select'"));
        break;

      case "delete":
        break;

      case "export":
        break;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $vocabulary = $form_state->getValue('taxonomy_select');
    $delimiter = $form_state->getValue('delimiter');
    $action = $form_state->getValue('action_item');

    switch ($action) {
      case "add":
        _import_terms_form($form, $form_state, $vocabulary);
        break;

      case "delete":
        // Call the delete function from the main module file.
        _delete_terms($vocabulary);
        break;

      case "export":
        // Call the export function from the main module file.
        _export_terms($vocabulary, $delimiter);
        break;
    }
  }

}
