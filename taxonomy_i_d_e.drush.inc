<?php

/**
 * @file
 * Taxonomy import/export/delete drush commands.
 */

require_once 'src/Form/TaxonomyIDEForm.php';

/**
 * Implements hook_drush_command().
 */
function taxonomy_i_d_e_drush_command() {
  $items = [];

  $items['taxonomy-export'] = [
    'description' => dt('Exports taxonomy from a csv.'),
    'aliases' => ['taxe'],
    'options' => [
      'taxonomy' => dt('The taxonomy you want to import into.'),
    ],
    'callback' => 'drush_taxonomy_export_generate',
  ];

  $items['taxonomy-delete'] = [
    'description' => dt('deletes terms from a taxonomy.'),
    'aliases' => ['taxd'],
    'options' => [
      'taxonomy' => dt('The taxonomy you want to delete terms of.'),
    ],
    'callback' => 'drush_taxonomy_delete_generate',
  ];

  $items['taxonomy-import'] = [
    'description' => dt('imports terms from a taxonomy. Run this using the command drush taxi --taxonomy="TAXONOMY_NAME" < ~/local_file_location/filename.csv'),
    'aliases' => ['taxi'],
    'options' => [
      'file' => dt('The location and name of the csv file.'),
      'taxonomy' => dt('The taxonomy you want to add terms to.'),
      'delimiter' => dt('The delimiter for parent child hierarchy.')
    ],
    'callback' => 'drush_taxonomy_import_generate',
  ];

  return $items;
}

/**
 * Callback function to generate terms for the requested taxonomy.
 */
function drush_taxonomy_export_generate() {

  $taxonomy = drush_get_option('taxonomy');
  $delimiter = drush_get_option('delimiter');

  if ($delimiter == NULL) {
    $delimiter = " ---> ";
  }

  // Export terms using function from main module file.
  _export_terms($taxonomy, $delimiter);

}

/**
 * Callback function to delete terms for the requested taxonomy.
 */
function drush_taxonomy_delete_generate() {

  $taxonomy = drush_get_option('taxonomy');

  // Delete terms using function from main module file.
  _delete_terms($taxonomy);

}

/**
 * Callback function to import terms for the requested taxonomy.
 */
function drush_taxonomy_import_generate() {

  // This is the taxonomy we will import into.
  $taxonomy = drush_get_option('taxonomy');

  // This is the full file path to the local file.
  $filedata = drush_get_option('file', 'php://stdin');

  // This is the delimiter.
  $delimiter = drush_get_option('delimiter');

  if ($delimiter == NULL) {
    $delimiter = " ---> ";
  }

  // This is just the filename of the local file.
  $filename = basename($filedata);

  // This is the target url for the file.
  $fileurl = 'public://import/' . $filename;

  // Check if a file already exists under that name and renames it accordingly.
  if (file_exists($fileurl)) {
    $number = 0;
    $original_fileurl = $fileurl;
    while (file_exists($fileurl)) {
      $fileurl = $original_fileurl . $number;
      $number++;
    }
  }

  // Save the file to the public directory.
  $handle = fopen($filedata, 'r');
  $file = file_save_data($handle, $fileurl);
  fclose($handle);

  // Validate the newly uploaded file.
  if ($handle = fopen($fileurl, 'rb')) {

    if ($line = fgetcsv($handle, 4096)) {

      // Validate the headers.
      if ($line[0] !== 'name') {
        drupal_set_message(t("@filename must have a column header value of 'name'.", ['@filename' => $filename]));
        return;
      }

      fclose($handle);
    }
    else {
      drupal_set_message(t("@filename is not a properly formatted csv file.", ['@filename' => $filename]));
      return;
    }
  }

  // Read in the file and import terms using function from main module file.
  _import_terms($fileurl, $taxonomy, $delimiter);

}
